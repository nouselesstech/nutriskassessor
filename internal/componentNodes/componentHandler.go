package componentNodes

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"nutRiskAssessor/internal/nutTui"
	"strconv"
	"github.com/google/uuid"
	"regexp"
)

type ComponentHandler struct {
	DataBase  *sql.DB
	Tui       *nutTui.NutTui
	CompTable ComponentTable
}

type ComponentTable struct {
	Components []ComponentNode
	Selection  int
}

func (c *ComponentHandler) HandleInput(cmd rune) {
	switch cmd {
	case 'c':
		c.Tui.WriteStatus("Component Menu")
	case 'g':
		c.CompTable = c.GetComponentNodes()
		c.CompTable.WriteTable(c)
	case 'n':
		c.NewNode()
	case '\r', 'i', 'j', 'k', 'l', 'd':
		c.TableNav(cmd)
	case 'q':
		c.Tui.RunState = false
	default:
		c.Tui.WriteStatus(fmt.Sprintf("Invalid command: %s", strconv.QuoteRune(cmd)))
	}
}

func (c *ComponentHandler) TableNav(cmd rune) {
	maxRows := c.Tui.TermY - 9
	switch cmd {
	case 'k':
		if c.CompTable.Selection < len(c.CompTable.Components) {
			c.CompTable.Selection++
		}
		c.CompTable.WriteTable(c)
	case 'i':
		if c.CompTable.Selection > 0 {
			c.CompTable.Selection--
		}
		c.CompTable.WriteTable(c)
	case 'l': 
		if (c.CompTable.Selection + maxRows) <= len(c.CompTable.Components) {
			c.CompTable.Selection = c.CompTable.Selection + maxRows
		}	
		c.CompTable.WriteTable(c)
	case 'j': 
		if (c.CompTable.Selection - maxRows) >= 0 {
			c.CompTable.Selection = c.CompTable.Selection  - maxRows
		}
		c.CompTable.WriteTable(c)
	case 'd':
		if len(c.CompTable.Components) > 0 {
			c.DeleteComponent(c.CompTable.Components[c.CompTable.Selection].Id)
			c.CompTable = c.GetComponentNodes()
		}
		c.CompTable.WriteTable(c)
	case '\r':
		c.GetComponentDetails(&c.CompTable.Components[c.CompTable.Selection])
	}
}

func (c* ComponentHandler) GetComponentDetails (component *ComponentNode) {
	c.Tui.ClearContent()

	// Write the nav
	c.Tui.AppTerm.Write([]byte("\n\033[0m___Component Information___ \n"))

	// Write the node
	c.Tui.AppTerm.Write([]byte(fmt.Sprintf("\033[0mId: %s\n", component.Id)))
	c.Tui.AppTerm.Write([]byte(fmt.Sprintf("\033[0mName: %s\n", component.Name)))
	c.Tui.AppTerm.Write([]byte(fmt.Sprintf("\033[0mDescription: %s\n", component.Description)))
	c.Tui.AppTerm.Write([]byte(fmt.Sprintf("\033[0mHIPAA: %d\n", component.Props.HIPAA)))


	// Write the status
	c.Tui.WriteStatus("Details Complete.")
	c.Tui.AppTerm.Write([]byte("\033[5,1H"))
}

func (c *ComponentHandler) DeleteComponent( id string ) {
	deleteString := fmt.Sprintf("DELETE FROM nodes WHERE Id = '%s'", id)
	preparedStmt, err := c.DataBase.Prepare(deleteString)
	if err != nil {
		panic(err)
	}
	_, err = preparedStmt.Exec()
	if err != nil {
		panic(err)
	}
	
	preparedStmt.Close()
}

func (c *ComponentHandler) GetComponentNodes() ComponentTable {
	returnComps := ComponentTable{
		Components: nil,
		Selection:  0,
	}

	rows, err := c.DataBase.Query("SELECT * FROM nodes WHERE Type = 'Component'")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		var sysRaComp ComponentNode
		var Props string
		err = rows.Scan(&sysRaComp.Id, &sysRaComp.Type, &sysRaComp.Name, &sysRaComp.Description, &Props)
		if err != nil {
			panic(err)
		}

		json.Unmarshal([]byte(Props), &sysRaComp.Props)
		returnComps.Components = append(returnComps.Components, sysRaComp)
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}

	return returnComps
}

func (t *ComponentTable) WriteTable(c *ComponentHandler) {
	c.Tui.ClearContent()

	// Let's calculate page size and location.
	maxRows := c.Tui.TermY - 9 
	var sliceStart int
	var sliceEnd int
	var page int

	if maxRows > len(t.Components) {
		sliceStart = 0
		sliceEnd = len(t.Components)

	} else {
		for x := 0; true; x++  {
			if ( t.Selection >= x*maxRows && t.Selection < (x+1)*maxRows ) {
				page = x
				break
			}
		}
		sliceStart = maxRows * page
		if maxRows * (page + 1) <= len(t.Components) {
			sliceEnd = maxRows * (page + 1)
		} else {
			sliceEnd = len(t.Components)
		}
	}

	c.Tui.AppTerm.Write([]byte("\033[0mNav: Up (i), Down (k), NextPage (l), Previous Page (j), select (enter)\n\n"))
	c.Tui.AppTerm.Write([]byte("\033[0m---- Component Table -----\n"))
	compSlice := t.Components[sliceStart:sliceEnd]
	for ndx, comp := range compSlice {
		if ndx + (page * maxRows) == t.Selection {
			c.Tui.AppTerm.Write([]byte("\033[1;37;41m"))
		} else {
			c.Tui.AppTerm.Write([]byte("\033[0m"))
		}

		compBytes, err := json.Marshal(comp.Props)
		if err != nil {
			panic(err)
		}
		str := fmt.Sprintf(
			"%s, %s, %s, %s \n",
			comp.Type,
			comp.Name,
			comp.Description,
			compBytes,
		)
		c.Tui.AppTerm.Write([]byte(str))
	}
	c.Tui.AppTerm.Write([]byte("\033[0m-------------------------"))
	c.Tui.WriteStatus("Results Complete.")
	c.Tui.AppTerm.Write([]byte("\033[5,1H"))
}

func (c *ComponentHandler) NewNode() {
	c.Tui.ClearContent()
	c.Tui.AppTerm.Write([]byte("Component Name"))
	name, err := c.Tui.AppTerm.ReadLine()
	if err != nil {
		panic(err)
	}

	c.Tui.AppTerm.Write([]byte("Description"))
	description, err := c.Tui.AppTerm.ReadLine()
	if err != nil {
		panic(err)
	}

	c.Tui.AppTerm.Write([]byte("HIPAA"))
	hipaa, err := c.Tui.AppTerm.ReadLine()
	if err != nil {
		panic(err)
	}

	hipaaInt, err := strconv.Atoi(hipaa)
	if err != nil {
		panic(err)
	}

	if hipaaInt == 0 || hipaaInt == 1 {
		node := ComponentNode{
			Id:          uuid.New().String(),
			Type:        "Component",
			Name:        name,
			Description: description,
			Props: ComponentProps{
				HIPAA: hipaaInt,
			},
		}
		c.WriteComponentNode(node)
		c.Tui.ClearContent()
		c.Tui.WriteStatus("Component saved.")
	} else {
		c.Tui.ClearContent()
		c.Tui.WriteStatus("Invalid input. HIPAA must be 1 or 0.")
	}
}

func CleanInput(input string) string {
	// Cleaners
	singleQuoteCleaner, err := regexp.Compile("'")
	if err != nil {
		panic(err)	
	}
	output := singleQuoteCleaner.ReplaceAllString(input, "''")
	
	// return
	return output
}

func (c *ComponentHandler) WriteComponentNode(newComp ComponentNode) {
	propStringBytes, err := json.Marshal(newComp.Props)
	if err != nil {
		panic(err)
	}
	propString := string(propStringBytes[:])
	insertStr := fmt.Sprintf(
		"INSERT INTO nodes VALUES ('%s', '%s', '%s', '%s', '%s')",
		CleanInput(newComp.Id),
		CleanInput(newComp.Type),
		CleanInput(newComp.Name),
		CleanInput(newComp.Description),
		CleanInput(propString),
	)

	fmt.Println(insertStr)

	insertStmt, err := c.DataBase.Prepare(insertStr)

	if err != nil {
		panic(err)
	}

	_, err = insertStmt.Exec()
	if err != nil {
		panic(err)
	}

	insertStmt.Close()
}
