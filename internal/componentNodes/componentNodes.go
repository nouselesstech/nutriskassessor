package componentNodes

type ComponentProps struct {
   HIPAA int
}

type ComponentNode struct {
    Id string
    Type string
    Name string
    Description string
    Props ComponentProps
}

