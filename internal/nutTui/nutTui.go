package nutTui

import (
	"fmt"

	"golang.org/x/term"
)

type NutTui struct {
	MenuState string
	RunState  bool
	AppTerm   *term.Terminal
	TermX     int
	TermY     int
}

func (n *NutTui) ClearScreen() {
	n.AppTerm.Write([]byte("\033[2J"))
}

func (n *NutTui) WriteMenu() {
	// Menu Positions
	topRow := fmt.Sprintf("\033[%d;%dH", 1, 1)

	// Clear and Write rows
	n.AppTerm.Write([]byte(topRow))
	n.AppTerm.Write([]byte("\033[2K"))
	n.AppTerm.Write([]byte("----------- nut Risk Assessor -------------"))
	n.AppTerm.Write([]byte("\n"))
	n.AppTerm.Write([]byte(n.GetBreadCrumbs()))
	n.AppTerm.Write([]byte("\n"))
	n.AppTerm.Write([]byte(n.GetMenu()))
	n.AppTerm.Write([]byte("\n"))
}

func (n *NutTui) GetMenu() string {
	switch n.MenuState {
	case "main":
		return "\033[0mCmds: (q)uit, (c)omponents"
	case "comp":
		return "\033[0mCmds: (g)et components, (n)ew component, (r)eturn, (s)earch, (q)uit"
	default:
		return "\033[0mCmd: (q)uit, (c)omponents"
	}
}

func (n *NutTui) GetBreadCrumbs() string {
	switch n.MenuState {
	case "main":
		return "Main Menu"
	case "comp":
		return "Main Menu >> Components"
	default:
		return "Main Menu"
	}
}

func (n *NutTui) WriteStatus(s string) {
	statusRow := fmt.Sprintf("\033[%d;%dH", n.TermY, 1)
	n.AppTerm.Write([]byte(statusRow))
	n.AppTerm.Write([]byte("\033[2K\033[0m"))
	n.AppTerm.Write([]byte(s))
}

func (n *NutTui) MenuResponse(r rune) {
	switch n.MenuState {
	case "main":
		switch r {
		case 'q':
			n.WriteStatus("Exiting application.")
			n.RunState = false
		case 'c':
			n.WriteStatus("Opening components.")
			n.MenuState = "comp"
		default:
			n.WriteStatus("Invalid command.")
		}
	case "comp":
		switch r {
		case 'q':
			n.WriteStatus("Exiting application.")
			n.RunState = false
		case 'g':
			n.WriteStatus("Getting components.")
		case 'n':
			n.WriteStatus("Creating a new component.")
		case 'r':
			n.WriteStatus("Returning to main menu.")
			n.ClearScreen()
			n.MenuState = "main"
		default:
			n.WriteStatus("Invalid command.")
		}
	default:
		n.WriteStatus("Unhandled menu state.")
	}
}

func (n *NutTui) ClearContent() {
	// Clear between menu/status bar
	row := 4
	topContentRow := fmt.Sprintf("\033[%d;%dH\033[0m", 4, 1)

	for row < n.TermY-1 {
		rowClear := fmt.Sprintf("\033[%d;%dH", row, 1)
		n.AppTerm.Write([]byte(rowClear))
		n.AppTerm.Write([]byte("\033[2K"))
		row++
	}

	n.AppTerm.Write([]byte(topContentRow))
}
