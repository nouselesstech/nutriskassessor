package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"nutRiskAssessor/internal/componentNodes"
	"nutRiskAssessor/internal/nutTui"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/term"
)

func main() {
	// Connect to the database
	db, err := sql.Open("sqlite3", "./RISKDB")
	if err != nil {
		panic(err)
	}

	// Get the Terminal/TUI strapped
	viewPort, err := term.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}
	appTerm := (term.NewTerminal(os.Stdin, ">"))
	termX, termY, err := term.GetSize(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}

	raTui := nutTui.NutTui{MenuState: "main", RunState: true, AppTerm: appTerm, TermX: termX, TermY: termY}
	raTui.ClearScreen()

	// Init handlers
	compHandler := componentNodes.ComponentHandler{DataBase: db, Tui: &raTui}

	for raTui.RunState == true {

		// Update the screen state
		raTui.WriteMenu()

		// Get the userinput
		lineReader := bufio.NewReader(os.Stdin)
		menuSelect, _, err := lineReader.ReadRune()
		if err != nil {
			panic(err)
		}

		// Update the tui status bar
		raTui.MenuResponse(menuSelect)

		// Take desired action
		switch raTui.MenuState {
		case "comp":
			compHandler.HandleInput(menuSelect)
			break
		default:
			break
		}
	}

	fmt.Print("\r\n")
	defer term.Restore(int(os.Stdin.Fd()), viewPort)
}
